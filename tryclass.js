function getDropDown() //gets the value of dropdown
{
    var txt = document.getElementById("identifier");
    var use = txt.options[txt.selectedIndex].text;
    return use;
}

function checktype() //check types from the dropdown selection that which object to instantiate
{
    var val = getDropDown();
    if (val == "SIMPLE TODO") {
        return 1;
    } else if (val == "Auto Done TODO") {
        return 2;
    }

    return 3;
}

function cleartext() //clears the text after todo item is created
{
    document.getElementById("myInput").value = "";

}

function randomcolors() {
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
} //assign the random colors

setInterval(function () {

    document.getElementById("formdiv").style.color = randomcolors();
    document.getElementById("formdiv").style.backgroundColor = randomcolors();
    document.body.style.backgroundColor = randomcolors();
}, 1000) //called after every 1 sec changes background color and div's color




class Basic //abstract class
{

    constructor(txtval) {
        this.textbox = txtval;
    }
    getVal() {
        return this.textbox;
    }
    createsNode() {


    }




}

class Simple extends Basic //simple type class
{
    constructor(txtval) {
        super(txtval);

    }

    createsNode() {
        var ul = document.getElementById("list");
        var li = document.createElement("li");
        li.id = "s";
        li.className = "first";
        li.appendChild(document.createTextNode(this.getVal()));
        var cb = document.createElement("input");
        cb.type = "checkbox";
        cb.className = "check1";
        cb.checked = false;
        cb.onchange = function print() {
            var t = this.parentElement;
            this.parentElement.parentElement.removeChild(this.parentElement);
            ul.appendChild(t);

       }

        li.appendChild(cb);
        ul.insertBefore(li, ul.childNodes[0]);

    }

}

class Auto extends Basic //autodone todo
{
    constructor(txtval) {
        super(txtval);

    }

    createsNode() {

        var ul = document.getElementById("list");
        var li = document.createElement("li");
        li.id = "s";
        li.className = "second";
        li.appendChild(document.createTextNode(this.getVal()));
        var cb = document.createElement("input");
        cb.type = "checkbox";
        cb.className = "check1";
        cb.checked = false;
        li.appendChild(cb);
        ul.insertBefore(li, ul.childNodes[0]);
        setTimeout(function () {
            cb.checked = true;
            var saveparent = cb.parentElement;
            cb.parentElement.parentElement.removeChild(cb.parentElement);
            ul.append(saveparent);

        }, 2000);



    }

}

class Reminder extends Basic //reminder todo
{
    constructor(txtval) {
        super(txtval);
    }

    createsNode() {

        var ul = document.getElementById("list");
        var li = document.createElement("li");
        li.id = "s";
        li.className = "third";
        li.appendChild(document.createTextNode(this.getVal()));
        var cb = document.createElement("input");
        cb.type = "checkbox";
        cb.className = "check1";
        cb.checked = false;
        li.appendChild(cb);
        ul.appendChild(li);
        setTimeout(function () {
            cb.checked = true;
            var saveparent = cb.parentElement;
            cb.parentElement.parentElement.removeChild(cb.parentElement);
            ul.insertBefore(saveparent, ul.childNodes[0]);

        }, 2000);

    }
}




function getText(val) //gets the text value from text box when enter is pressed and then checks the type and create the todo
{
    var txt;
    if (event.keyCode == 13) {
        txt = document.getElementById("myInput").value;
        if (checktype() == 1) {
            let b = new Simple(txt);
            if (txt != "") {
                b.createsNode();
            }
            cleartext();

        } else if (checktype() == 2) {
            let a = new Auto(txt);
            if (txt != "") {
                a.createsNode();
            }
            cleartext();
        } else {
            let r = new Reminder(txt);
            if (txt != "") {
                r.createsNode();
            }
            cleartext();
        }
    }
}